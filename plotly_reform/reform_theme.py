from plotly.graph_objs import Layout, Font, XAxis, YAxis, Legend, Marker, Line


def reform_blog_theme(myfig):
    """
    A function to theme a plot as a Reform blog chart. Resizes
    to the current width of the Reform blog, 650 pixels.

    Inputs:
        myfig: a plotly.graph_objs.Figure

    Returns:
        None

    The figure is modified in place.
    """

    # layout updates
    layout_reform = {'titlefont': Font(size=14),
                     'font': Font(size=10),
                     'legend': Legend(x=0.01,
                                      y=1.025,
                                      font=Font(size=8),
                                      bgcolor='rgb(244, 224, 237)'),
                     'paper_bgcolor': 'rgb(243, 245, 241)',
                     'plot_bgcolor': 'rgb(243, 245, 241)',
                     'bargap': 0.2,
                     'bargroupgap': 0,
                     'width': 650,
                     'height': 400,
                     'hidesources': True}

    for key, value in myfig['layout'].items():
        if key.startswith('yaxis'):
            layout_reform[key] = YAxis(titlefont=Font(size=12),
                                       tickfont=Font(size=10),
                                       gridcolor='rgb(255, 255, 255)',
                                       zerolinecolor='rgb(204, 204, 204)')
        elif key.startswith('xaxis'):
            layout_reform[key] = XAxis(titlefont=Font(size=12),
                                       showgrid=False,
                                       zeroline=False,
                                       tickfont=Font(size=10))

    myfig['layout'].update(layout_reform)

    # data updates
    line_colors = [
        'rgb(130, 0, 83)', 'rgb(214, 12, 140)', 'rgb(184, 144, 194)', "rgb(99, 97, 154)", "rgb(115, 175, 182)", "rgb(145, 165, 165)"]
    for k, v in enumerate(myfig['data']):
        k_cycle = k % len(line_colors)
        myfig['data'][k].update(marker=Marker(color=line_colors[k_cycle]),
                                line=Line(color=line_colors[k_cycle]))