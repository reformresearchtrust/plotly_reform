from setuptools import setup


def readme():
    with open('README.md') as f:
        return f.read()

setup(name='plotly_reform',
      version='0.1.0',
      description='Reform theming for plotly charts',
      long_description=open('README.md').read(),
      url='https://bitbucket.org/reformresearchtrust/plotly_reform',
      author='jzuccollo',
      author_email='james.zuccollo@reform.co.uk',
      packages=['plotly_reform'],
      install_requires=[
          'plotly'
      ],
      test_suite='nose.collector',
      tests_require=['nose'],
      zip_safe=False,
      package_data={'plotly_reform': ['img/dummy_plot.png',
                                      'img/themed_dummy_plot.png']})
