#Plotly Reform theme

[Plotly](http://plot.ly/) is a useful tool for sharing plots but it is difficult to theme the plots through the Python API. There is a _Reform_ blog theme in the web interface and this package replicates it in Python.

##Installation

The package depends on the following packages:

 - `plotly`

They should be automatically installed by `pip`.

Clone the repository locally with 

    git clone https://MY_USERNAME@bitbucket.org/reformresearchtrust/plotly_reform.git

and install with 

    cd plotly_reform
    pip install .

If you wish to automatically update the package with each git commit then use

    pip install -e .

##Usage

The package provides a function to update a `plotly.graph_obj.Figure` in place. For example, a default plotly chart looks like this:

![Plotly defaults](https://bitbucket.org/reformresearchtrust/plotly_reform/raw/master/plotly_reform/img/dummy_plot.png =100x)

Wrapping with the _Reform_ theme using

    from plotly_reform import reform_blog_theme

    reform_blog_theme(figure)
    py.plot(figure, filename="themed_dummy_plot")

returns the themed and resized plot:

![Reform blog theme](https://bitbucket.org/reformresearchtrust/plotly_reform/raw/master/plotly_reform/plotly_reform/img/themed_dummy_plot.png =70x)
